# pesr <img src='man/figures/logo.png' align="right" height="139" />

<!-- badges: start -->

<!-- badges: end -->

The pesR package provides functionality for interacting with APIs produced by the NASA Socioeconomic Data and Applications Center (SEDAC).

Users are able to select and view Web Mapping Services (WMS) of more than 400 NASA SEDAC data sets.  

Users are able to interactively create subnational, regional, or international polygons of arbitrary size and complexity to request population estimates of Age and Sex Structure, Age Pyramids; which are downloadable as CSV data tables, or PNG Pyramid Plots. These estimates are calculated from the [Gridded Population of the World Version 4 Basic Demographic Characteristics Dataset](https://doi.org/10.7927/H46M34XX)using SEDAC's Population Estimation Service, Version 3 ([PES-v3](https://doi.org/10.7927/H4DR2SK5)) API, which is the namesake of the pesR package.

## Installation

You can install the released version of commonCodes from
[GitLab](https://gitlab.com/) with:

``` r
devtools::install_gitlab("dante-sttr/pesr", dependencies=TRUE)
```

### Installation With Windows

To install R packages over Git on a Windows system, you must install
Rtools first. The latest version of Rtools is available
[here](https://cran.r-project.org/bin/windows/Rtools/). Furthermore, you
may experience difficulty installing R packages over Git if you utilize
a Windows machine on a network with Active Directory or shared network
drives. To enable proper package installation under these circumstances
please follow [this
guide](https://dante-sttr.gitlab.io/dante-vignettes/windows-pkg-inst/windows-pkg-inst.html).


## Example

To get a list of available WMS layers for use as a basemap call the 'getSedacLayers()' function. 

``` r
library(pesr)
layer_names<-getSedacLayers()
```

this returns a data table with a list of layer names and abstracts. For example

     ##     name                                                              abstract
     ## 13	anthromes:anthromes-anthropogenic-biomes-world-v2-2000	            The Anthropogenic Biomes v2: 2000 map layer displays anthropogenic transformations within the terrestrial biosphere caused by sustained direct human interaction with ecosystems, including agriculture and urbanization c. 2000. See more information at https://doi.org/10.7927/H4D798B9.
     ## 14	crop-climate:crop-climate-effects-climate-global-food-production	Effects of Climate Change on Global Food Production from SRES Emissions and Socioeconomic Scenarios, v1 (1970-2080): Maize, Rice, and Wheat displays the potential implications of climate change on staple crop production yields. See more information at http://dx.doi.org/10.7927/H4JM27JZ.
     ## 15	energy:energy-pop-exposure-nuclear-plants-locations_plants	        Population Exposure Estimates in Proximity to Nuclear Power Plants, Locations, v1 (1956-2012) displays locations of nuclear power plants. See more information at http://dx.doi.org/10.7927/H4WH2MXH.
     ## 16	epi:epi-environmental-performance-index-2010	                    Environmental Performance Index, 2010 Release (1994-2009) displays the total EPI which ranks 163 countries on environmental performance based on 25 indicators grouped within 10 policy categories. See more information at http://dx.doi.org/10.7927/H4D21VHT.

Through examining this list, users can select a layer to use as a basemap for their exploration of population estimates.

The 'layer_name' of a specific item can be passed to the 'launchAgePyramidApp()' function.

``` r
launchAgePyramidApp(layer_name="epi:epi-environmental-performance-index-2010")
```

This will trigger the construction of a Shiny App with the aforementioned WMS service as its basemap.

Within the Shiny App, users can leverage interactive drawing tools which will automatically trigger a request which produces an Age Pyramid Table and Plot.
