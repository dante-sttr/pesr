#' @title Create Shiny Leaflet App with WMS Basemap from NASA SEDAC and PESv3
#'
#' @description Define parameters for the creation of a Shiny Leaflet app which
#'     allows user generated polygons as input into requests for Age Pyramids from
#'     the GPW4.10 data collection. The app also allows users to select a basemap
#'     from the SEDAC data collection of WMS services.
#'
#' @param layer_name This is the input layer name type \code{character} from SEDAC WMS services
#'                   to use as a Leaflet basemap. The default value is coded NULL, if not
#'                   specified then UN Adjusted Population Counts for year 2020 from the
#'                   GPWv4 Revision 11 data set is used. Users can select other basemaps
#'                   by first running the getSedacLayers function and reviewing
#'                   the resulting dataframe.
#' @param shiny_params Name of the return variable type \code{list} which contains a
#'                     variable of type \code{list} called 'ui' and another variable
#'                     of type \code{function} which are parameters for running a Shiny app
#'
#' @return object of class 'list'
#' @import leaflet
#' @export


launchAgePyramidApp<-function(layer_name=NULL,shiny_params){
  if(is.null(layer_name)){layer_name<-"gpw-v4:gpw-v4-population-count-adjusted-to-2015-unwpp-country-totals-rev11_2020"}
  layer_names<-getSedacLayers()
  wms_layer<-layer_names[which(layer_names==layer_name),1]
  # parse the wms_legend call
  wms_legend_layer<-substr(wms_layer,regexpr(':', wms_layer)[1]+1,1000)
  # create the shiny app
  shiny::shinyApp(
    # Define UI
    ui<-shiny::fluidPage(shiny::sidebarLayout(position="left",
      shiny::sidebarPanel(
        htmltools::h3("The pesR shiny app returns Age Pyramid Tables and Plots based
            on user drawn rectangles or polygons.", align = "center"),
        htmltools::h4("To use the application: select from the draw tools on the left
           panel of the Leaflet map, draw a rectangle or polygon and wait
           for the SEDAC server to process the request. If the request is
           successful, then a data table and accompanying age pyramid plot
           will be returned to the Plot and Data Table tabs for viewing and
           for download as PNG or CSV respectively.",align = "left"),
        htmltools::h5("The estimates returned are based on data from SEDAC's Gridded
           Population of the World, Version 4 Revision 11 (GPWv4.11) Basic
           Demographic Characteristics data set.", align = "left"),
        htmltools::h6(htmltools::strong(shiny::textOutput("requestCode"),align ="center"))
        ),
      shiny::mainPanel(
        shinyjs::useShinyjs(),
        shiny::tabsetPanel(
          shiny::tabPanel("Map",leaflet::leafletOutput("mymap",height=600)),
          shiny::tabPanel("Plot", shiny::downloadButton("agePlotDownload",
                                                        "Download Plot"),
                          shiny::plotOutput("agePlot")),
          shiny::tabPanel("Data Table", shiny::downloadButton("ageTableDownload",
                                                              "Download Data"),
                          shiny::tableOutput("ageTable"))
          )))),
    # Define server logic
    server<-function(input, output) {
      # instantiate the download buttons in disabled state
      shinyjs::disable(id="agePlotDownload")
      shinyjs::disable(id="ageTableDownload")
      output$mymap<-leaflet::renderLeaflet(
        # create leaflet map
        leaflet_map<-leaflet::leaflet(wms_layer,
                                      options = leaflet::leafletOptions(minZoom = 2)) %>%
          # add drawing tools
          leaflet.extras::addDrawToolbar(
            targetGroup = "draw",
            polylineOptions = FALSE,
            markerOptions= FALSE,
            circleOptions = FALSE,
            circleMarkerOptions = FALSE,
            singleFeature = TRUE
          ) %>%
          # add WMS layer
          leaflet::addWMSTiles(
            "https://sedac.ciesin.columbia.edu/geoserver/gwc/service/wms?service=WMS",
            layers = wms_layer,
            options = leaflet::WMSTileOptions(format = "image/png", transparent = TRUE)
          )%>%
          leaflet::setView( lng = -20
                            , lat = 0
                            , zoom = 2 ) %>%
          leaflet::setMaxBounds( lng1 = -180
                                 , lat1 = -65
                                 , lng2 = 180
                                 , lat2 = 90 )%>%
          # add WMS legend
          leaflet.extras::addWMSLegend(
            uri = paste0(
              "https://sedac.ciesin.columbia.edu/geoserver/",
              "wms?request=GetLegendGraphic&format=image%2Fpng&style",
              "=",wms_legend_layer,":default",
              "&width=15&height=15&legend_options=border:false;mx:0.05;my:0.02;dx:0.2;dy:0.07;",
              "fontSize:11;bandInfo:false;&strict=false"),
            position = "bottomright"
          )
      )
      pyramid_react <- shiny::reactiveVal()
      shiny::observeEvent(input$mymap_draw_new_feature,{
        pyramid_react(NULL)
        feature <- input$mymap_draw_new_feature
        coordinates<-feature$geometry$coordinates
        i<-1
        coord_pairs <- character()
        for (coords in coordinates){
          for(coord in coords){
            coord_pair<-paste0("[",coord[1],",",coord[2],"]")
            coord_pairs<-paste(coord_pairs,coord_pair,sep=",")
          }
        }
        coord_pairs<-sub(".","",coord_pairs)
        output$requestCode<-shiny::renderText(NULL)
        shiny::withProgress(message = 'Calculation in progress',
                            detail = 'Thank you for your patience, currently querying 28 raster data sets', {
          for(i in 1:70){
            # Long Running Task
            Sys.sleep(1)
            # Update progress
            shiny::incProgress(1/80)
          }
        pyramid_return<-estimateAgePyramid(coord_pairs)
        pyramid_react(pyramid_return)
        if("message" %in% pyramid_return){
        output$requestCode<-shiny::renderText(pyramid_return$message)
        }
        else{
          # send plot and table to the UI
          output$requestCode<-shiny::renderText("Request Succeeded, Please See Plot and Data Table Tabs")
          output$agePlot<-shiny::renderPlot(pyramid_return$pyramid_plot)
          shinyjs::enable(id="agePlotDownload")
          output$agePlotDownload <- shiny::downloadHandler(
            filename = function() { paste(input$dataset, '.png', sep='') },
            content = function(file) {
              ggplot2::ggsave(file, plot = pyramid_return$pyramid_plot,
                              device = "png")
            })
          output$ageTable<-shiny::renderTable(pyramid_return$pyramid_table)
          shinyjs::enable(id="ageTableDownload")
          output$ageTableDownload <- shiny::downloadHandler(
            filename = function() { paste(input$dataset, '.csv', sep='') },
            content = function(file) {
              utils::write.csv(pyramid_return$pyramid_table, file)
            }
          )
        }})
        })
    })
}

