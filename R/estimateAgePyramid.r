#' @title Estimate Age Pyramid
#'
#' @description Make a request to the SEDAC PES version 3 service
#'     and receive a table of population estimates and an age pyramid plot
#'
#' @param coord_pairs Input data frame of coordinate
#' @param pyramid_return Name for the returned \code{list}
#'        If the request was successful then the list will
#'        contain a pyramid_table of type \code{data_frame}
#'        and a pyramid_plot of type \code{plot}, otherwise
#'        the list will contain message of type \code{character}
#'        which is the API's error message
#'
#' @return object of class 'list'
#' @export

estimateAgePyramid<-function(coord_pairs,pyramid_return){
  # bind global variables
  Sex<-NULL
  Age<-NULL
  Population<-NULL
  # function that removes negative values in the ticks of the age pyramid and adds thousands separators
  absComma <- function(x) {scales::comma(abs(x))}
  # formulate request
  pes_url<-paste0("https://sedac.ciesin.columbia.edu/arcgis/rest/services/",
                  "sedac/pesv3Broker/GPServer/pesv3Broker/execute")
  input_pes_data<-paste0('{"polygon": ','[',coord_pairs,'],',
                         '"variables": ["gpw-v4-basic-demographic-characteristics-rev10_a000-004ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a000-004mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a005-009ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a005-009mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a010-014ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a010-014mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a015-019ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a015-019mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a020-024ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a020-024mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a025-029ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a025-029mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a030-034ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a030-034mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a035-039ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a035-039mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a040-044ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a040-044mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a045-049ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a045-049mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a050-054ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a050-054mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a055-059ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a055-059mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a060-064ft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a060-064mt-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a065plusft-count",',
                         '"gpw-v4-basic-demographic-characteristics-rev10_a065plusmt-count"],"statistics": ["SUM"],',
                         '"requestID": "123456789"}')
  # execute the request
  response<-jsonlite::fromJSON(
    httr::content(
      httr::POST(
        pes_url,
        query=list(f="json", Input_Data=input_pes_data),
        encode="form",
        config = httr::config(ssl_verifypeer = FALSE)
      ),
      as="text"
    ), flatten = TRUE
  )
  # disable scientific notation
  options(scipen=999)
  # need to check resultCode in order to confirm the request was successful
  # if not successful then send a message to the user
  response_df<-response$results
  result_code<-response_df$value.resultCode
  message<-response_df$value.message
  if (!grepl("Succeeded",message)){
    pyramid_return<-list("message"=message)
    return(pyramid_return)
  }else{
    response_df<-response_df[,grepl("value.estimates",colnames(response_df))]
    colnames(response_df)<-sub("value.estimates.","",colnames(response_df))
    response_df<-as.data.frame(t(stats::reshape(response_df, varying=colnames(response_df), sep=".", direction="long")))
    colnames(response_df)<-as.character(unlist(response_df["time",]))
    response_df<-response_df[!(row.names(response_df) %in% c("id","time")), ]
    response_df<-utils::type.convert(response_df, numerals="no.loss")
    response_df<-response_df[order(rownames(response_df)),]
    response_df$SHORTTITLE<-as.character(response_df$SHORTTITLE)
    # plot age pyramid from results
    pyramid_df<-response_df
    pyramid_df$Sex<-gsub( "s .*$", "", pyramid_df$SHORTTITLE)
    pyramid_df$Age<-substr(pyramid_df$SHORTTITLE,nchar(pyramid_df$Sex)+3, nchar(pyramid_df$SHORTTITLE))
    pyramid_df$Age<-ifelse(pyramid_df$Age == "0 - 4", "00 - 04", pyramid_df$Age)
    pyramid_df$Age<-ifelse(pyramid_df$Age == "5 - 9", "05 - 09", pyramid_df$Age)
    pyramid_df$Population<-ifelse(pyramid_df$Sex == "Male",
                                  -1*pyramid_df$SUM, pyramid_df$SUM)
    pyramid_plot<-ggplot2::ggplot(pyramid_df, ggplot2::aes(x=Age, y=Population, fill=Sex)) +
      ggplot2::geom_bar(stat = "identity") +
      ggplot2::scale_y_continuous(limits=c(-max(abs(pyramid_df$Population)),
                                  max(abs(pyramid_df$Population))),labels=absComma)+
      ggplot2::coord_flip()+
      ggplot2::theme_minimal() +
      ggplot2::scale_fill_brewer(palette="Set1")+
      ggplot2::ggtitle("Age Pyramid 2010")+
      ggplot2::theme(plot.title = ggplot2::element_text(hjust = 0.5))
    # create and parse a return data frame
    return_df<-pyramid_df
    return_df$Population<-ifelse(return_df$Sex == "Male",
                                 -1*return_df$Population, pyramid_df$Population)
    keeps<-c("Sex","Age","Population")
    return_df<-return_df[ ,keeps,drop=FALSE]
    pyramid_return<-list("pyramid_table"=return_df,"pyramid_plot"=pyramid_plot)
    return(pyramid_return)
  }
}
