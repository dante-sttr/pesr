#' @title Grab List of SEDAC WMS Layers
#'
#' @description Make a get capabilities request to the SEDAC production Geoserver and
#' parse the results into a table of Names and Abstracts for each WMS layer
#'
#' @param layer_names User provided variable to hold the returned \code{data frame}
#'
#' @return object of class 'data frame'
#' @export

getSedacLayers<-function(layer_names){
  getNames<-function(xml){
    name<-xml2::xml_text(xml2::xml_find_first(xml,".//Name"))
    abstract<-xml2::xml_text(xml2::xml_find_first(xml,".//Abstract"))
    return(data.frame(name=name,abstract=abstract))
  }
  request<-paste0("https://sedac.ciesin.columbia.edu/geoserver/",
                  "ows?service=wms&version=1.1.1&request=GetCapabilities")
  xml_read<-xml2::read_xml(request)
  xml_capability<-xml2::xml_find_all(xml_read,".//Capability")
  xml_layers<-xml2::xml_find_all(xml_capability,".//Layer")
  layer_names<-do.call(rbind, lapply(xml_layers,getNames))
  return(layer_names)
}
